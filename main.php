<?php


function bfs($labyrinth, $start_node, $end_node) {
    $queue = new SplQueue();
    $queue->enqueue([$start_node[0], $start_node[1], 0]);

    $visited = array();
    $visited[implode("_", $start_node)] = True;

    while (!$queue->isEmpty()) {
        $curr = $queue->dequeue();
        $x = $curr[0];
        $y = $curr[1];
        $cost = $curr[2];
        echo $x . " " . $y . " " . $cost . "\r\n";

        if ([$x, $y] == $end_node) {
            return $cost;
        }

        if ($x < sizeof($labyrinth) - 1 and
            !array_key_exists(implode("_", [$x + 1, $y]), $visited) and
            $labyrinth[$x + 1][$y] != 0) {
            $queue->enqueue([$x + 1, $y, $cost + $labyrinth[$x + 1][$y]]);
            $visited[implode("_", [$x + 1, $y])] = True;
        }

        if ($x > 0 and
            !array_key_exists(implode("_", [$x - 1, $y]), $visited) and
            $labyrinth[$x - 1][$y] != 0) {
            $queue->enqueue([$x - 1, $y, $cost + $labyrinth[$x - 1][$y]]);
            $visited[implode("_", [$x - 1, $y])] = True;
        }

        if ($y < sizeof($labyrinth[0]) - 1 and
            !array_key_exists(implode("_", [$x, $y + 1]), $visited) and
            $labyrinth[$x][$y + 1] != 0) {
            $queue->enqueue([$x, $y + 1, $cost + $labyrinth[$x][$y + 1]]);
            $visited[implode("_", [$x, $y + 1])] = True;

        }

        if ($y > 0 and
            !array_key_exists(implode("_", [$x, $y - 1]), $visited) and
            $labyrinth[$x][$y - 1] != 0) {
            $queue->enqueue([$x, $y - 1, $cost + $labyrinth[$x][$y - 1]]);
            $visited[implode("_", [$x, $y - 1])] = True;

        }

    }

    return -1;
}


function start() {
    $labyrinth = [
        [1, 2, 3, 4],
        [1, 2, 3, 4],
        [1, 0, 3, 4],
        [1, 0, 3, 4]
    ];

    return bfs($labyrinth, [3, 0], [3, 2]);
}

echo start();
